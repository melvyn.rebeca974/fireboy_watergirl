
class Player extends Collider{
    constructor(x, y, type){
        super();
        this.x = x;
        this.y = y;
        this.speed= 10;
        this.boost= 0;
        this.type= type;
        this.width = 65 + "px";
        this.height = 91 + "px";
        this.element = null;
        this.createPlayer(type);
        setInterval(function(e){

            e.gravity()
            if(e.boost>0){

                e.boost=Math.round((e.boost-0.1) * 10) / 10
            }//else if(e.boost<0){
            //     e.boost+=0.1;
            // }
        }
        ,100, this);


    } 
    speedAdd(direction){
        if (direction == "right"  && this.boost < 1) {
            this.boost = Math.round((this.boost+0.1) * 10) / 10
            console.log(this.boost);

        }
        if (direction == "left"  && this.boost > -1) {
            this.boost = Math.round((this.boost-0.1) * 10) / 10
            console.log(this.boost);

        }
        
    }
    speedRemove(){
        // let count = 0;
        console.log(this.boost);
        if (this.boost >= 0) {
            
            setInterval(() => {
                // console.log("time");
                
                this.boost -= 0.1;
                // console.log(this.boost);
            }, 1000);
            this.boost = 0;
        }else{
            clearInterval();
            console.log("pass");
        }

    }
    move(direction) {
        this.colide();
        if (direction === "left" && !this.isCollide.includes("left")) {
            this.x -= this.speed * this.boost;
            this.element.style.transform = "rotateY(3.142rad)";
            
        }else if(direction === "right" && !this.isCollide.includes("right")){
            this.x += this.speed * this.boost;
            this.element.style.transform = "rotateY(0)";

        }else if(direction === "jump" && !this.isCollide.includes("top")){
            this.y += 150;
            this.x += 50;
        }
         
        // console.log(this.isCollide);
        this.element.style.left = this.x + "px";
        
    }
    createPlayer(type){
        let player1 = document.createElement("div");
        player1.classList.add("perso");
        player1.classList.add(type);
        player1.style.left = this.x + "px";
        player1.style.bottom = this.y + "px";
        player1.style.width = this.width;
        player1.style.height = this.height;
        document.body.appendChild(player1);
        this.element = player1;
    }
}
