class Collider {
    static objects = [];
    constructor(){
        this.isCollide = []; 
        this.gravityValue = 9;

        Collider.setObjects(this);
    }
    static setObjects(obj){
        Collider.objects.push(obj);
    }
    static getObjects() {
        console.log(Collider.objects);
    }
    gravity(){
        
        this.colide();
        if (!this.isCollide.includes("bottom")) {
            
            this.y -= this.gravityValue;
            this.element.style.bottom = this.y + "px";
        }
     
    }
    colide(){

        this.isCollide = [];
        var PBG = {"x":this.x, "y":this.y}
        var PHG =  {"x":this.x, "y":this.y + parseInt(this.height.replace("px", ""))}
        var PBD =  {"x":this.x + parseInt(this.width.replace("px", "")), "y":this.y}
        var PHD = {
            "x":this.x + parseInt(this.width.replace("px", "")), "y":this.y + parseInt(this.height.replace("px", ""))
         }

        Collider.objects.forEach(obj => {
            if (this!=obj) {
                
                var OBG = {"x":obj.x, "y":obj.y}
                var OHG =  {"x":obj.x, "y":obj.y + parseInt(obj.height.replace("px", ""))}
                var OBD =  {"x":obj.x + parseInt(obj.width.replace("px", "")), "y":obj.y}
                var OHD = {"x":obj.x + parseInt(obj.width.replace("px", "")), "y":obj.y + parseInt(obj.height.replace("px", ""))}
                    

                // collision vers la droite
                if (
                    (
                        (PBD.x + this.speed > OBG.x) 
                        &&
                        (
                            (PBD.y < OHG.y)
                            &&
                            (PHD.y > OBG.y)
                        )
                        && !(PBG.x > OBG.x + this.speed)
                    )
                )
                {
                    // correct x
                    if(PBD.x != OBG.x){
                        this.x = OBG.x - parseInt(this.width.replace("px", ""));
                        this.element.style.left = this.x + "px";
                    }

                    this.isCollide.push("right");
                }
                // collision vers la gauche
                if 
                (
                    (
                        (PBG.x - this.speed < OBD.x) 
                        &&
                        (
                            (PBG.y < OHD.y)
                            &&
                            (PHG.y > OBD.y)
                        )
                        && !(PBD.x < OBD.x - this.speed)
                    )
                )
                {
                    // correct x
                    if(PBG.x != OBD.x){
                        this.x = OBD.x;
                        this.element.style.left = this.x + "px";
                    }
                    this.isCollide.push("left");
                }


                // collision vers le bas
                if (
                    (
                        (PBG.y - this.gravityValue < OHG.y)
                        &&
                        (
                            (PBD.x > OBG.x)
                            &&
                            (PBG.x < OBD.x)
                        )
                    && 
                        !(PBG.y < OHG.y - this.gravityValue)
                    )
                ) 
                {   
                    
                    // correct y
                    if(PBG.y != OHD.y){
                        this.y = OHD.y;
                        this.element.style.bottom = this.y + "px";
                    }

                    this.isCollide.push("bottom");
                }

                // collision vers le haut
                if (
                    (
                        (PHG.y + this.gravityValue > OBG.y)
                        &&
                        (
                            (PHD.x > OHG.x)
                            &&
                            (PHG.x < OHD.x)
                        )
                    && 
                        !(PHG.y > OBG.y + this.gravityValue)
                    )

                )
                {
                    // correct y
                    if(PHG.y != OBD.y){
                        this.y = OBD.y - parseInt(this.height.replace("px", ""));
                        this.element.style.bottom = this.y + "px";
                    }
                                        
                    this.isCollide.push("top");
                }
            

               
                // enter dans une porte
            }
    
        });
    }
}