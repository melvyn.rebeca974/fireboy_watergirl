class Ennemy extends Collider{
    constructor(x, y, width, height ,type){
        super();
        this.x = x;
        this.y = y;
        this.speed = null;
        this.boost = 1;
        this.width = width + "px";
        this.height = height + "px";
        this.type = type;
        this.element = null;
        this.createEnnemy(type);
        this.changeSpeed();
        setInterval(function(e){
            e.gravity()
            // e.move();
        }
        ,100, this);
        
        this.loop();
        
    }
    getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }
    loop() {
        var rand = this.getRandomArbitrary(1,5);
        setTimeout(function(e) {
            e.changeSpeed();
            e.loop();
        }, rand*1000, this);
    }
    changeSpeed(){
        this.speed = this.getRandomArbitrary(-10,10);
    }
    move(){
       

        
        this.x += this.speed;

        this.element.style.left = this.x + "px";
    }
    createEnnemy(type){
        let ennemy = document.createElement("div");
        ennemy.classList.add("perso");
        ennemy.classList.add(type);
        ennemy.style.left = this.x + "px";
        ennemy.style.bottom = this.y + "px";
        ennemy.style.width = this.width;
        ennemy.style.height = this.height;
        document.body.appendChild(ennemy);
        this.element = ennemy;
    }
}