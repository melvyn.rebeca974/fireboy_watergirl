class Plateform extends Collider{
    constructor(x, y, width, height, matiere){
        super();
        this.x = x;
        this.y = y;

        if(width == "full") this.width = document.querySelector("#level").clientWidth + "px";
        else this.width = width + "px";

        if(height == "full") this.height = document.querySelector("#level").clientHeight + "px";
        else this.height = height + "px";
        this.matiere = matiere;

        this.element = null;
        this.createPlateform();

    }
    createPlateform(){
        let plateform1 = document.createElement("div");
        plateform1.classList.add("platforme");
        plateform1.style.left = this.x + "px";
        plateform1.style.bottom = this.y + "px";
        plateform1.style.width = this.width;
        plateform1.style.height = this.height;
        plateform1.style.backgroundImage = "url('assets/img/" + this.matiere + ".jpg')";

        document.querySelector("#level").appendChild(plateform1);
        this.element = plateform1;
    }
}