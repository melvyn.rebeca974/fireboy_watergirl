window.addEventListener("load", function(){
    initLevel(1);
    
    
});


function initLevel(level){
    if(level == 1){
        // init player
            var player1 = new Player(10, 30, "fire");
            setInterval(function(e){
                e.move("right");
            }
            ,100, player1);
            //var player2 = new Player(10, 530, "water");
            
            //var ennemy = new Ennemy(500, 330, 70, 140, "guigui");

            var widthScreen = document.querySelector("#level").clientWidth;
            var heightScreen = document.querySelector("#level").clientHeight;
            
            var borderBottom = new Plateform(0, 0, "full", 30, "floor_lava");
            var borderLeft = new Plateform(-30, 0, 30, "full", "floor_lava");
            var borderRight = new Plateform(widthScreen, 0, 30, "full", "floor_lava");
            var boderTop = new Plateform(0, heightScreen, "full", 80, "floor_lava");
            
            //var plateform1 = new Plateform(0, 450, 200, 80, "floor_lava");
            // var plateform2 = new Plateform(0, 100, 200, 80, "floor_stone");
            // var plateform3 = new Plateform(320, 250, 400, 80, "floor_rock");
            // var plateform4 = new Plateform(850, 100, 250, 80, "floor_lava");
            // var plateform5 = new Plateform(850, 450, 250, 80, "floor_stone");

            // var doorRed = new Plateform(950, 530, 70, 140, "door_red");
            // var doorBlue = new Plateform(950, 180, 70, 140, "door_blue");
           //var plateformTop = new Plateform(0, 750, 200, 80);
            // var plateformWall = new Plateform(0, 0, 20, "full");
            // init Gravity
            Player.getObjects();
    }


    // Déplacement
    const keysPressed = {};

    function handleKeyDown(e){
        keysPressed[e.key] = true;

        if(keysPressed['d']){
            player1.speedAdd("right");
        }
        if(keysPressed['q']){
            player1.speedAdd("left");
        }
        if(keysPressed['z']){
            player1.move("jump");
        }
        if(keysPressed['ArrowRight']){
            player2.move("right");
        }
        if(keysPressed['ArrowLeft']){
            player2.move("left");
        }
        if(keysPressed['ArrowRight'] && keysPressed['d']){
            player1.move("right");
            player2.move("right");
        }
        if(keysPressed['ArrowLeft'] && keysPressed['q']){
            player1.move("left");
            player2.move("left");
        }
    }

    function handleKeyUp(e){

        delete keysPressed[e.key];
        
    }

    document.addEventListener('keydown', handleKeyDown);
    document.addEventListener('keyup', handleKeyUp);

    // document.addEventListener("keydown", function(e){
    //     e.preventDefault();
    //     if(e.code == "KeyA"){
    //         player1.move("left");
    //     }else if(e.code == "KeyD"){
    //         player1.move("right");
    //     }

    //     if(e.code == "ArrowLeft"){
    //         player2.move("left");
    //     }else if(e.code == "ArrowRight"){
    //         player2.move("right");
    //     }
    //     // console.log(player1.x);
    // });

}
